import grp  # unix only
import getpass
from invoke import task

DOCKER_TAG = "latest"
DOCKER_IMAGE = f"docker.com/jrabbit/impatient-apt:{DOCKER_TAG}"

@task
def build(c) -> None:
    "builds docker container"
    user = getpass.getuser()
    if user not in grp.getgrnam("docker").gr_mem:
        c.run(f"sudo docker build -t {DOCKER_IMAGE} .")
    else:
        c.run(f"docker build -t {DOCKER_IMAGE} .")