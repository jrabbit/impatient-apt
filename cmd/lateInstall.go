package cmd

import (
	"container/list"
	"fmt"
	"github.com/godbus/dbus/v5"
	"log"
	"os/exec"

	"github.com/spf13/cobra"
)

var packagesToInstall *list.List

// lateInstallCmd represents the lateInstall command
var lateInstallCmd = &cobra.Command{
	Use:   "lateInstall",
	Short: "the main case for this tool",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("lateInstall called")
		// just called
		packagesToInstall = list.New()
		if IsLocked(){
			lateInstall(args)
		}else {
			// just run the apt command directly!
			exec.Command("apt", "install", "cowsay")
		}

		// https://github.com/purpleidea/mgmt/blob/master/engine/resources/packagekit/packagekit.go
		//ch := make(chan *dbus.Signal, PkBufferSize)
		//var signals = []string{"Package", "ErrorCode", "Finished", "Destroy"}
		//removeSignals, err := obj.matchSignal(ch, interfacePath, PkIfaceTransaction, signals)
		//if err != nil {
	//		return err
//		}
//		defer removeSignals()

		conn, err := dbus.SystemBus()
		if err != nil {
			log.Println(err)
		}
		var interfacePath dbus.ObjectPath
		bus := conn.Object("org.freedesktop.PackageKit", "/org/freedesktop/PackageKit")
		call := bus.Call(fmt.Sprintf("%s.CreateTransaction", "org.freedesktop.PackageKit"), 0).Store(&interfacePath)
		log.Printf("txn id: %s", interfacePath)
		if call != nil {
			log.Fatal(call)
		}
		call2 := bus.Call("Transaction.InstallPackages", 0, 0, []string{"nano;;;"})
		if call2.Err != nil {
			log.Fatal(call2.Err)
		}
	},
}

func init() {
	rootCmd.AddCommand(lateInstallCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// lateInstallCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// lateInstallCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

func IsLocked() bool {
	// is apt locked?
	cmd := exec.Command("fuser", "-v", "/var/lib/dpkg/lock-frontend")
	err := cmd.Run()
	if err != nil {
		log.Println(err)
	}
	exit := cmd.ProcessState.ExitCode()
	log.Printf("fuser exit code: %v", exit)
	if exit == 1 {
		return false
	}else{
		return true
	}
}

func lateInstall(packagesNamed []string){
	//
	packagesToInstall.PushBack(packagesNamed)
}
