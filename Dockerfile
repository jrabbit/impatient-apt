FROM golang:buster as build

LABEL maintainer="Jack Laxson <jack+impatient_apt@getpizza.cat>"

WORKDIR /go/src/app
ADD . /go/src/app

RUN go build -o /go/bin/app

FROM debian:buster
LABEL maintainer="Jack Laxson <jack+impatient_apt@getpizza.cat>"
RUN apt update && apt install psmisc && rm -rf /var/cache/apt
COPY --from=build /go/bin/app /
CMD ["/app"]